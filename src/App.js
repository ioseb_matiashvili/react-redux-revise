import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import Landing from "./components/Landing";
import Posts from "./components/Posts";
import axios from "axios";
import "./App.css";

const App = () => {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const res = await axios.get(
          "https://jsonplaceholder.typicode.com/posts"
        );
        //console.log(res.data);
        setPosts(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchPosts();
  }, []);
  return (
    <Provider store={store}>
      <Router>
        <Route exact path='/' component={Landing} />
        <Route exact path='/posts' posts={posts} component={Posts} />
      </Router>
    </Provider>
  );
};

export default App;
