import { GET_POSTS, GET_POSTS_FAILED } from "../actions/actions";

const initialState = {
  posts: [],
  loading: true,
  error: null,
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_POSTS:
      return {
        ...state,
        posts: payload,
        error: false,
        loading: false,
      };
    case GET_POSTS_FAILED:
      return {
        ...state,
        posts: [],
        error: payload,
      };
    default:
      return {
        ...state,
      };
  }
}
