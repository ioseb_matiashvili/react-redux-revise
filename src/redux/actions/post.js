import { GET_POSTS, GET_POSTS_FAILED } from "./actions";
import axios from "axios";

export const getPosts = () => async (dispatch) => {
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/posts");

    dispatch({
      type: GET_POSTS,
      payload: res.data,
    });
    console.log(res.data);
  } catch (error) {
    dispatch({ type: GET_POSTS_FAILED });
  }
};
