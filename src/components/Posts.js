import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { getPosts } from "../redux/actions/post";
import { connect } from "react-redux";

const Posts = ({ post: { posts }, getPosts }) => {
  //const [posts, setPosts] = useState([]);
  useEffect(() => {
    getPosts();
  }, [getPosts]);

  const newPosts = posts.slice(0, 10);
  const smallTitlePosts = posts.filter(
    (postItem) => postItem.title.length < 15
  );

  return (
    <div>
      <h1>Posts</h1>
      <Link to='/'>Home</Link>
      <div className='posts'>
        {posts
          ? smallTitlePosts.map((postItem) => (
              <div key={postItem.id} className='post-card'>
                <p>{postItem.title}</p>
                <h4>{postItem.body}</h4>
              </div>
            ))
          : "error"}
      </div>
    </div>
  );
};

Posts.propTypes = {
  getPosts: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  post: state.post,
});

export default connect(mapStateToProps, { getPosts })(Posts);
