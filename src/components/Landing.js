import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Landing = (props) => {
  return (
    <div>
      <h1>Landing Page</h1>
      <Link to='/posts'>Posts</Link>
    </div>
  );
};

Landing.propTypes = {};

export default Landing;
